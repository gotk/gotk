package utils

import (
	log "github.com/sirupsen/logrus"
	"testing"
)

func TestPluck(t *testing.T) {
	type S struct {
		Id uint64
	}

	arr := []S{
		{
			Id: 1,
		},
		{
			Id: 2,
		},
	}

	out := PluckUint64(arr, "Id")
	log.Infof("out %+v", out)

	out = PluckUint64("", "Id")
	log.Infof("out %+v", out)
}

func TestKeyBy(t *testing.T) {
	type T struct {
		Id   uint32
		Name string
	}

	list := []T{
		{
			Id:   1,
			Name: "hello",
		},
		{
			Id:   2,
			Name: "world",
		},
	}

	var m map[uint32]T
	KeyByMap(list, "Id", &m)

	log.Infof("m is %+v", m)
}
