package core

type ServerConfig struct {
	ServerName   string `yaml:"server-name"`
	ServerListen string `yaml:"server-listen"`
	Environment  string `yaml:"environment"`
}
